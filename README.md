# create-cloud-deploy-release

The `create-cloud-deploy-release` GitLab Component creates a Cloud Deploy release to manage the deployment of an application to one or more Google Kubernetes Engine (GKE), Anthos, or Cloud Run targets.

## Prerequisites
- The Service Account/Workload Identity used to create the release should have proper permissions configured. Please check [Authorization](#authorization) section.
- This Component depends on the existence of a Cloud Deploy delivery pipeline that is configured for the targets to which the application will be deployed.

## Usage

``` yaml
include:
  - component: gitlab.com/quanzhang/cloud-deploy-component-prod/create-cloud-deploy-release@v0.1.0
    inputs:
      workload_identity_provider: "//iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>" 
      audience: "//iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>" 
      project_id: "<project-id>"
      name: "demo-release"
      delivery_pipeline: "demo-pipeline"
      region: "us-central1"
      images: "img1=path/to/img1,img2=path/to/img2"
```


## Inputs
### Authentication

#### [Service Account Key JSON][sa-private-key]
-   `credentials_json_env_var`: (Optional) The env var containing the full credentials json (without `$`). The credentials json will be used to authenticate to Google Cloud services if provided. Follow the [instructions][instructions] to setup GitLab CI/CD env var.

#### [Workload Identity Federation][wif]
-   `workload_identity_provider`: (Optional) The full identifier of the Workload Identity Provider, including the project number, pool name, and provider name. This must be the full identifier which includes all parts:

    ``` yaml
        //iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>
    ```

-   `service_account`: (Optional) Email address or unique identifier of the Google Cloud Service Account for which to impersonate and generate credentials if provided. The Component attempts to authenticate through direct Workload Identity Federation otherwise.

-   `audience`: (Optional) The value for the audience (aud) parameter in the generated GitLab Component OIDC token.


### Create Cloud Deploy Release

-   `name`: (Required) The name for the release.

-   `project_id`: (Required) The ID of the Google Cloud project in which to deploy the service.
    - e.g. `my-project`

-   `region`: (Required) The region of the delivery pipeline. 
    - e.g.  `us-central1`

-   `delivery_pipeline` (Required): The [delivery pipeline][cd-pipeline] to use
    for the release.
    - e.g.  `my-release-pipeline
    `
-   `source`: (Required) The location of the files to be included in the
    release; typically application configuration manifests. Default to `.`.
    - e.g. `web/`

-   `images`: (Optional) The details of the application image(s) to be released. 

    - In the format: `image1=path/to/image1:v1@sha256:45db24,image2=path/to/image2:v1@sha256:d2fd24` (no space).

-   `description`: (Optional) The a description of the release.

-   `to_target`: (Optional) The specific target to deliver into upon release creation
    - e.g. `test-target`

-   `skaffold_file`: (Optional) The path of the skaffold file absolute or relative
    to the source directory.
    - e.g. `skaffold.yaml`

-   `skaffold_version`: (Optional) The version of the Skaffold binary.
    - e.g. `2.8`

-   `initial_rollout_annotations`: (Optional) The annotations to apply to the initial rollout when creating the release.
    
    - In the format: `annotation1=val1,annotation2=val2` (no space).

-   `initial_rollout_labels`: (Optional) The labels to apply to the initial rollout when creating the release.
     
    - In the format: `label1=val1,label2=val2` (no space).

-   `initial_rollout_phase_id`: (Optional) The phase to start the initial rollout at when creating the release.
    - e.g. `stable`

-   `stage`: (Optional) The GitLab CI/CD stage, default to `deploy`.

## Authorization

To use the Component, the provided Service Account/Workload Identity should have the following minimum roles:
  - Cloud Deploy Releaser (`roles/clouddeploy.releaser`)
  - Cloud Storage Admmin (`roles/storage.admin`)
  - Service Account User (`roles/roles/iam.serviceAccountUser`)

**Note:** This is in addition to any other roles required by Cloud Deploy itself, [GKE][gke] and [Cloud Run][cloud-run] authorization. For details, please check: [Cloud Deploy Service Accounts][cd-service-accounts].

[cd-pipeline]: https://cloud.google.com/deploy/docs/terminology#delivery_pipeline
[wif]: https://cloud.google.com/iam/docs/workload-identity-federation
[sa-private-key]: https://cloud.google.com/iam/docs/keys-create-delete#iam-service-account-keys-create-console
[instructions]: https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui
[cd-service-accounts]: https://cloud.google.com/deploy/docs/cloud-deploy-service-account
[gke]: https://cloud.google.com/deploy/docs/deploy-app-gke#before-you-begin
[cloud-run]: https://cloud.google.com/deploy/docs/deploy-app-run#before-you-begin


